<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => ['web', 'XSS']], function () {

	Route::get('/', function () { return view('welcome'); });
	Route::get('/blog', function () { return view('blog'); });
	Route::get('/blog/{slug}', function () { return view('blog-detail'); });
	Route::get('/book', function () { return view('book'); });
	Route::get('/book/{slug}', function () { return view('book-detail'); });
	Route::get('/about-us', function () { return view('about'); });
	Route::get('/contact-us', function () { return view('contact'); });
	Route::get('/login', function () { return view('login'); });
	Route::get('/register', function () { return view('register'); });
	Route::get('/lupa-password', function () { return view('lupa'); });
	Route::get('/admin', 'AdminController@getLogin');
	Route::post('/admin', 'AdminController@doLogin');
});

Route::group(['middleware' => ['web', 'auth', 'XSS', 'admin']], function () {

	Route::get('/admin/home', 'AdminController@index');
	Route::get('/admin/logout', 'AdminController@Logout');
	Route::resource('/admin/user', 'UserController');
	Route::resource('/admin/kategori', 'KategoriController');
	Route::resource('/admin/book', 'BookController');
	Route::resource('/admin/artikel', 'BlogController');
	Route::resource('/admin/transaksi', 'PinjamController');
	Route::resource('/admin/testimoni', 'TestiController');
});