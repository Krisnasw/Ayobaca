<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Validator;
use Auth;

class AdminController extends Controller
{
    //
    public function index()
    {
    	return view('admin.index');
    }

    public function getLogin()
    {
    	return view('admin.login');
    }

    public function doLogin(Request $request)
    {
    	$valid = Validator::make(
    		$request->all(), array(
    			'username' => 'required',
    			'password' => 'required'
    			));

    	if ($valid->passes()) {
    		# code...
    		if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
    			# code...
    			Alert::success('Selamat Datang di Administrator', 'Success!', 'Success');
    			return redirect('/admin/home');
    		} else {
    			Alert::error('Username atau Password Salah', 'Error!', 'Error');
    			return redirect()->back()->withInput($request->all());
    		}
    	} else {
    		Alert::info('Username atau Password Masih Kosong', 'Info!', 'Info');
    		return redirect()->back()->withInput($request->all());
    	}
    }

    public function Logout()
    {
        if (Auth::logout()) {
            # code...
            Alert::error('Logout Gagal!', 'Error!', 'Error');
            return redirect()->back();
        } else {
            Alert::success('Logout Berhasil!', 'Success!', 'Success');
            return redirect()->back();
        }
    }
}
