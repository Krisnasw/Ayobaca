<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use Alert;
use File;
use Validator;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Kategori::orderBy('id', 'desc')->get();
        return view('admin.kategori.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make(
            $request->all(), array(
                'kat_name' => 'required'
                ));

        $data = new Kategori();

        if ($valid->passes()) {
            # code...
            $data['kat_name'] = $request->kat_name;
            $data['slug'] = str_slug($request->kat_name);
            $data['dilihat'] = 0;
            $data->save();

            if ($data) {
                # code...
                Alert::success('Kategori Berhasil Ditambah', 'Success!', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Kategori Gagal Ditambah', 'Error!', 'Error');
                return redirect()->back()->withInput($request->all());
            }
        } else {
            Alert::info('Data yang anda isi kurang lengkap', 'Info!', 'Info');
            return redirect()->back()->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make(
            $request->all(), array(
                'kat_name' => 'required'
                ));

        $data = Kategori::findOrFail($id);

        if ($valid->passes()) {
            # code...
            $data['kat_name'] = $request->kat_name;
            $data['slug'] = str_slug($request->kat_name);
            $data->save();

            if ($data) {
                # code...
                Alert::success('Kategori Berhasil Diupdate', 'Success!', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Kategori Gagal Diupdate', 'Error!', 'Error');
                return redirect()->back()->withInput($request->all());
            }
        } else {
            Alert::info('Data yang anda isi kurang lengkap', 'Info!', 'Info');
            return redirect()->back()->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = Kategori::findOrFail($id);
        $del->delete();

        if ($del) {
            # code...
            Alert::success('Kategori Berhasil Dihapus', 'Success!', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Kategori Gagal Dihapus', 'Error!', 'Error');
            return redirect()->back();
        }
    }
}
