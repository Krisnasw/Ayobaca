<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Alert;
use Hash;
use Validator;
use File;

class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function layout()
    {
        return view('admin.user.index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = User::orderBy('id', 'desc')->where('role', '=', 'member')->get();
        return view('admin.user.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make(
                $request->all(), array(
                    'nis' => 'required|numeric',
                    'name' => 'required',
                    'kelas' => 'required',
                    'email' => 'required',
                    'role' => 'required',
                    'image' => 'image|mimes:jpg,jpeg,png',
                    'password' => 'required'
                    ));

        $data = new User();
        $cek = User::select('email')->get();

        foreach ($cek as $key) {
            $email = $key['email'];
        }

        if ($valid->passes()) {
            # code...
            if ($request->email != $email) {
                # code...
                if ($request->hasFile('image')) {
                    # code...
                    $data['image'] = $this->savePhoto($request->file('image'));
                    $data['image'] = $data['image'];
                    $data['nis'] = $request->nis;
                    $data['name'] = $request->name;
                    $data['kelas'] = $request->kelas;
                    $data['email'] = $request->email;
                    $data['role'] = $request->role;
                    $data['password'] = Hash::make($request->password);
                    $data->save();

                    if ($data) {
                        # code...
                        Alert::success('User Berhasil Ditambah', 'Success!', 'Success');
                        return redirect()->back();
                    } else {
                        Alert::error('Gagal Tambah User', 'Error!', 'Error');
                        return redirect()->back();
                    }
                } else {
                    $data['image'] = 'images/user/user.jpg';
                    $data['nis'] = $request->nis;
                    $data['name'] = $request->name;
                    $data['kelas'] = $request->kelas;
                    $data['email'] = $request->email;
                    $data['role'] = $request->role;
                    $data['password'] = Hash::make($request->password);
                    $data->save();

                    if ($data) {
                        # code...
                        Alert::success('User Berhasil Ditambah', 'Success!', 'Success');
                        return redirect()->back();
                    } else {
                        Alert::error('Gagal Tambah User', 'Error!', 'Error');
                        return redirect()->back();
                    }
                }
            } else {
                Alert::info('Email Telah Terdaftar', 'Info!', 'Info');
                return redirect()->back()->withInput($request->all());
            }
        } else {
            Alert::info('Data Yang Anda Isi Tidak Lengkap', 'Info!', 'Info');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make(
                $request->all(), array(
                    'nis' => 'numeric',
                    'image' => 'image|mimes:jpg,jpeg,png',
                    ));

        $data = User::findOrFail($id);

        if ($valid->passes()) {
            # code...
            if ($request->email != $data['email']) {
                # code...
                if ($request->hasFile('image')) {
                    # code...
                    $data['image'] = $this->deletePhoto($data['image']);
                    $data['image'] = $this->savePhoto($request->file('image'));
                    $data['image'] = $data['image'];
                    $data['nis'] = $request->nis;
                    $data['name'] = $request->name;
                    $data['kelas'] = $request->kelas;
                    $data['email'] = $request->email;
                    $data['role'] = $request->role;
                    $data['password'] = Hash::make($request->password);
                    $data->save();

                    if ($data) {
                        # code...
                        Alert::success('User Berhasil Diupdate', 'Success!', 'Success');
                        return redirect()->back();
                    } else {
                        Alert::error('Gagal Update User', 'Error!', 'Error');
                        return redirect()->back();
                    }
                } else {
                    $data['image'] = 'images/user/user.jpg';
                    $data['nis'] = $request->nis;
                    $data['name'] = $request->name;
                    $data['kelas'] = $request->kelas;
                    $data['email'] = $request->email;
                    $data['role'] = $request->role;
                    $data['password'] = Hash::make($request->password);
                    $data->save();

                    if ($data) {
                        # code...
                        Alert::success('User Berhasil Diupdate', 'Success!', 'Success');
                        return redirect()->back();
                    } else {
                        Alert::error('Gagal Update User', 'Error!', 'Error');
                        return redirect()->back();
                    }
                }
            } else {
                Alert::info('Email Telah Terdaftar', 'Info!', 'Info');
                return redirect()->back()->withInput($request->all());
            }
        } else {
            Alert::info('Data Yang Anda Isi Tidak Lengkap', 'Info!', 'Info');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = User::findOrFail($id)->delete();
        $deleteImage = $this->deletePhoto($data['image']);
        $data->delete();

        if ($data) {
            # code...
            Alert::success('User Berhasil Dihapus', 'Success!', 'Success');
            return redirect()->back();
        } else {
            Alert::error('User Gagal Dihapus', 'Error!', 'Error');
            return redirect()->back();
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'user';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $data['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $data['image'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}
