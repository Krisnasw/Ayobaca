@include('header')

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Ayobaca.com - Blog</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li class="active">Blog</li>
            </ul>
        </div><!--/container-->
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="blog_masonry_3col">
        <div class="container content grid-boxes">
            <div class="grid-boxes-in">
                <img class="img-responsive" src="assets/img/main/img3.jpg" alt="">
                <div class="grid-boxes-caption">
                    <h3><a href="#">Unify Clean and Fresh Fully Responsive Template</a></h3>
                    <ul class="list-inline grid-boxes-news">
                        <li><span>By</span> <a href="#">Kathy Reyes</a></li>
                        <li>|</li>
                        <li><i class="fa fa-clock-o"></i> July 06, 2014</li>
                        <li>|</li>
                        <li><a href="#"><i class="fa fa-comments-o"></i> 06</a></li>
                    </ul>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>

        </div><!--/container-->
    </div>
    <!--=== End Content Part ===-->

    @include('footer')