<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{!! Auth::user()->name !!}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="{{ url('/admin/home') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('/admin/user') }}">
                <i class="fa fa-user"></i> <span>Data User</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('/admin/kategori') }}">
                <i class="fa fa-bars"></i> <span>Kategori Buku</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('/admin/book') }}">
                <i class="fa fa-book"></i> <span>Data Buku</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('/admin/transaksi') }}">
                <i class="fa fa-arrow-circle-left"></i> <span>Data Transaksi</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('/admin/artikel') }}">
                <i class="fa fa-comment"></i> <span>Data Artikel</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('/admin/testimoni') }}">
                <i class="fa fa-star-o"></i> <span>Data Testimoni</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>