      @include('admin.header')
      <!-- Left side column. contains the logo and sidebar -->
      @include('admin.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Administrator - Data User
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/admin/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data User</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <!-- left column -->
            <div class="col-md-6" style="margin-left: 25%;">

              <!-- Input addon -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Tambahkan User</h3>
                </div>
                {!! Form::open(array('action' => 'UserController@store', 'class' => 'form-horizontal', 'method' => 'POST', 'files' => true)) !!}
                <div class="box-body">
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-primary">NIS</button>
                    </div>
                    <input type="number" class="form-control" name="nis" placeholder="Masukkan NIS Anda" value="{!! old('nis') !!}">
                  </div>
                  <br />
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-default">Password</button>
                    </div>
                    <input type="password" class="form-control" name="password" placeholder="Masukkan Password Anda" value="{!! old('password') !!}">
                  </div>
                  <br />
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-warning">Nama</button>
                    </div>
                    <input type="text" class="form-control" name="name" placeholder="Masukkan Nama Anda" value="{!! old('name') !!}">
                  </div>
                  <br />
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-info">Kelas</button>
                    </div>
                    <input type="text" class="form-control" name="kelas" placeholder="Masukkan Kelas Anda" value="{!! old('kelas') !!}">
                  </div>
                  <br />
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-success">Email</button>
                    </div>
                    <input type="email" class="form-control" name="email" placeholder="Masukkan Email Anda" value="{!! old('email') !!}">
                  </div>
                  <br />
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-primary">Role User</button>
                    </div>
                    <select class="form-control" name="role">
                      <option value="member">Member</option>
                      <option value="admin">Admin</option>
                    </select>
                  </div>
                  <br />
                  <div class="input-group">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-danger">Foto Profil</button>
                    </div><!-- /btn-group -->
                    <input type="file" class="form-control" name="image">
                  </div><!-- /input-group -->
                  <br />
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="button" onclick="reset();" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                </div><!-- /.box-footer -->
                {!! Form::close() !!}
              </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
          </div>   <!-- /.row -->

          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data User</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>NIS</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Kelas</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key)
                      <tr>
                        <td>{!! $key['nis'] !!}</td>
                        <td>{!! $key['name'] !!}</td>
                        <td>{!! $key['email'] !!}</td>
                        <td>{!! $key['kelas'] !!}</td>
                        <td>
                          <button class="btn btn-primary" data-toggle="modal" data-target="#edit{{{ $key['id'] }}}">Edit</button>
                          <button class="btn btn-danger" data-toggle="modal" data-target="#delete{{{ $key['id'] }}}">Delete</button>
                        </td>
                      </tr>
                      <!-- Edit modal -->
                      <div id="edit{{{ $key['id'] }}}" class="modal fade">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header bg-warning">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h6 class="modal-title">Edit Data</h6>
                            </div>
                            {!! Form::open(array('files' => true, 'method' => 'PATCH', 'route' => array('user.update', $key['id']))) !!}
                            <div class="modal-body">
                              <div class="input-group">
                                  <div class="input-group-btn">
                                    <button type="button" class="btn btn-info">Nomor Induk Siswa</button>
                                  </div>
                                <input type="text" class="form-control" name="nis" value="{!! $key['nis'] !!}" readonly>
                              </div>
                              <br />
                              <div class="input-group">
                                  <div class="input-group-btn">
                                    <button type="button" class="btn btn-info">Password</button>
                                  </div>
                                <input type="password" class="form-control" name="password" value="{!! $key['password'] !!}">
                              </div>
                              <br />
                              <div class="input-group">
                                  <div class="input-group-btn">
                                    <button type="button" class="btn btn-info">Nama Lengkap</button>
                                  </div>
                                <input type="text" class="form-control" name="name" value="{!! $key['name'] !!}">
                              </div>
                              <br />
                              <div class="input-group">
                                  <div class="input-group-btn">
                                    <button type="button" class="btn btn-info">Kelas</button>
                                  </div>
                                <input type="text" class="form-control" name="kelas" value="{!! $key['kelas'] !!}">
                              </div>
                              <br />
                              <div class="input-group">
                                  <div class="input-group-btn">
                                    <button type="button" class="btn btn-info">Email</button>
                                  </div>
                                <input type="text" class="form-control" name="email" value="{!! $key['email'] !!}">
                              </div>
                              <br />
                              <div class="form-group">
                                  <select class="select2-metronic select2me form-control" name="role">
                                    <option value="member">Member</option>
                                    <option value="admin">Admin</option>
                                  </select>
                              </div>
                              <div class="form-group">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px;">
                                    @if(empty($key['image']))
                                      <img src="{{ asset('images/user/user.jpg') }}" alt=""/>
                                    @else
                                      <img src="{{ asset($key['image']) }}" alt=""/>
                                    @endif
                                    </div>
                                    <div class="input-group">
                                      <div class="input-group-btn">
                                        <button type="button" class="btn btn-danger">Gambar</button>
                                      </div><!-- /btn-group -->
                                      <input type="file" class="form-control" name="image">
                                    </div><!-- /input-group -->
                                  </div>
                              </div>
                            </div>

                            <div class="modal-footer">
                            {!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
                              <button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
                            </div>
                            {!! Form::close() !!}
                          </div>
                        </div>
                      </div>
                      <!-- /Edit modal -->
                            <div id="delete{{{ $key['id'] }}}" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header bg-danger">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h6 class="modal-title">Hapus Data</h6>
                                  </div>

                                  <div class="modal-body">
                                    <h6 class="text-semibold">Apakah Anda Yakin Ingin Menghapus, <i> {!! $key['name'] !!} </i></h6>
                                    <p></p>
                                    <p>NB : Data tidak dapat dikembalikan jika sudah dihapus.</p>
                                  </div>

                                  <div class="modal-footer">
                                    {!! Form::open(array('method' => 'DELETE', 'route' => array('user.destroy', $key['id']))) !!}
                                      {!! Form::submit("Ya", array('class' => 'btn btn-danger')) !!}
                                    {!! Form::close() !!}
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                        <!-- /End Edit -->
                            <div id="show{{{ $key['id'] }}}" class="modal fade">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header bg-primary">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h6 class="modal-title">Lihat Data</h6>
                                  </div>

                                  <div class="modal-body">
                                    <div class='box-body'>
                                      <img class="img-responsive pad" src="{{ asset($key['image']) }}" alt="Photo">
                                    </div><!-- /.box-body -->
                                  </div>

                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                    @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>NIS</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Kelas</th>
                        <th>Aksi</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      @include('admin.footer')