<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title>Ayobaca.com</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
    <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
    <!--[if lt IE 9]><link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms-ie8.css"><![endif]-->

    <!-- CSS Page Style -->
    <link rel="stylesheet" href="assets/css/pages/page_search.css">
    <link rel="stylesheet" href="assets/css/pages/page_log_reg_v1.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
    <link rel="stylesheet" href="assets/css/theme-skins/dark.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">
</head>

<body>

<div class="wrapper">
    <!--=== Header ===-->
    <div class="header">
      <div class="container">
          <!-- Logo -->
          <a class="logo" href="index.html">
              <img src="assets/img/logo1-default.png" alt="Logo">
          </a>
          <!-- End Logo -->

          <!-- Toggle get grouped for better mobile display -->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="fa fa-bars"></span>
          </button>
          <!-- End Toggle -->
      </div><!--/end container-->

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
          <div class="container">
              <ul class="nav navbar-nav">
                  <!-- Home -->
                  <li>
                      <a href="{{ url('/') }}">
                          Home
                      </a>
                  </li>
                  <!-- End Home -->

                  <!-- Buku -->
                  <li>
                      <a href="{{ url('/book') }}">
                          Buku
                      </a>
                  </li>
                  <!-- End Buku -->

                  <!-- Blog -->
                  <li>
                      <a href="{{ url('/blog') }}">
                          Blog
                      </a>
                  </li>
                  <!-- End Blog -->

                  <!-- Portfolio -->
                  <li>
                      <a href="{{ url('/about-us') }}">
                          Tentang Kami
                      </a>
                  </li>
                  <!-- End Portfolio -->

                  <!-- Features -->
                  <li>
                      <a href="{{ url('/contact-us') }}">
                          Kontak Kami
                      </a>
                  </li>
                  <!-- End Features -->

                  <!-- Shortcodes -->
                  <li class="mega-menu-fullwidth">
                      <a href="{{ url('/login') }}">
                          Login
                      </a>
                  </li>
                  <!-- End Shortcodes -->

                  <!-- Fak -->
                  <li>
                    <a href="{{ url('/register') }}">
                      Register
                    </a>
                  </li>
                  <!-- End Fak -->

                  <!-- Search Block -->
                  <li>
                      <i class="search fa fa-search search-btn"></i>
                      <div class="search-open">
                          <div class="input-group animated fadeInDown">
                              <input type="text" class="form-control" placeholder="Search">
                              <span class="input-group-btn">
                                  <button class="btn-u" type="button">Go</button>
                              </span>
                          </div>
                      </div>
                  </li>
                  <!-- End Search Block -->
              </ul>
          </div><!--/end container-->
      </div><!--/navbar-collapse-->
    </div>
    <!--=== End Header ===-->