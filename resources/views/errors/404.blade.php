<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Page Not Found!</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link href='{{ asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro|Indie+Flower') }}' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('assets/css/robot.min.css') }}">
  </head>

  <body>

  <div class="ghost">
    <span class="text">404</span>
    <div class="eye"></div>
    <div class="eye two"></div>
    
    <div class="mouth">
      <!--<div class="tonge"></div>-->
    </div>
    
    <div class="corner"></div>
    <div class="corner two"></div>
    <div class="corner three"></div>
    <div class="corner four"></div>
    
    <div class="over"></div>
    <div class="over two"></div>
    <div class="over three"></div>
    <div class="over four"></div>
    
    <div class="shadow"></div>
  </div>

  <div class="main">
    <h2>Page not found</h2>
    <h6>The page you are looking for might have been removed, had its name changed,<br />or is temporarily unavailable.</h6>
    <a href="{{ url('/') }}" style="text-decoration: none;"> Back To Home </a>
  </div>

  </body>
</html>