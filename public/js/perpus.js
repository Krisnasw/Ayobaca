Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");

new Vue({
	el: '#example1',

	data: {
        items: []
    },

	ready : function() {
  		this.getUserData();
  	},

	method	: {
		getUserData: function () {
          this.$http.get('/admin/user').then((response) => {
            this.$set('items', response.data.data.data);
          });
          console.log('getUserData');
        },
	}
});