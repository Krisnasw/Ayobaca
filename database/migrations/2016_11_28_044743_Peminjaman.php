<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Peminjaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('peminjaman', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id');
            $table->integer('book_id');
            $table->enum('status', ['pinjam', 'kembali', 'rusak', 'telat']);
            $table->date('tgl_pinjam');
            $table->date('tgl_kembali');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('peminjaman');
    }
}
