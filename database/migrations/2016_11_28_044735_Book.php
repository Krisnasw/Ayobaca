<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Book extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('buku', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kat');
            $table->string('judul');
            $table->string('slug');
            $table->text('keterangan');
            $table->string('pengarang');
            $table->date('tahun_terbit');
            $table->string('penerbit');
            $table->integer('jumlah');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('buku');
    }
}
